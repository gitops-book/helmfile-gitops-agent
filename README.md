# helmfile-gitops-agent

![Version: 1.1.34](https://img.shields.io/badge/Version-1.1.34-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v0.171.0](https://img.shields.io/badge/AppVersion-v0.171.0-informational?style=flat-square)

A GitOps agent for deploying Helmfiles continuously.

**Homepage:** <https://gitlab.com/gitops-book/helmfile-gitops-agent>

## Source Code

* <https://gitlab.com/gitops-book/helmfile-gitops-agent>

## What is this?

A CronJob cloning a repo and running `helmfile apply` in one namespace.

## Who is this for?

This is a simplistic GitOps agent.
It is suitable if you fulfill these criteria:

1. You are **mainly managing Helm charts**.
    * Remember though that [Helmfile can partially deal with Kustomize, too](https://helmfile.readthedocs.io/en/latest/#helmfile-kustomize).
1. You **cannot install Kubernetes operators** into your cluster.
    * You might e.g. have a platform team managing a cluster for you, giving you limited access to only a few namespaces.
      You cannot install operators in such a scenario because you are **not allowed to install CRDs**.

The first criterion is not necessary, the second one is:
**If you are able to install operators into your cluster, do go with a mature, CNCF-graduated solution like [Flux](https://landscape.cncf.io/?project=graduated&selected=flux) or [Argo](https://landscape.cncf.io/?project=graduated&selected=argo)!**

### Prerequisites

* You need a repo with at least one Helmfile in it.
  (We'll call it "config repo".)
    * The repo needs to be accessible via SSH.
      (No https currently.)
* You need a Kubernetes cluster and a target namespace.
  (Helmfile GitOps Agent runs **once per namespace**.)

## Usage

### ⚙ Installing

**Choose a namespace** where to install the chart.
Make sure that the Helmfiles in your config repo only deploy Helm releases to this namespace!

Switch to your target namespace and run this command (replace `REPO_SSH_URL` with your config repo's SSH URL):

```sh
helm install helmfile-gitops-agent \
  oci://quay.io/gitops-book/helmfile-gitops-agent \
  --set gitClone.repoUrl=REPO_SSH_URL
```

The output will contain a public key and instructions.
Follow the instructions to add this key as a deploy key to your config repo.

### 💣 Uninstalling

```sh
helm delete helmfile-gitops-agent
```

**❎ This will not automatically delete the Helm releases you installed from your config repo!**
To do that, run `helmfile destroy` *after* deleting the Helmfile GitOps Agent release.

### 🚢 Deploying stuff

Commit and push changes to your config repo.

### 🔥 Deleting stuff

**🚨 Deleting Helm releases requires two steps:**

1. Mark your release with `installed: false`.
   Commit this change, push it, wait for reconciliation.
1. *Optional but recommended*:
   Remove the release from the Helmfile.

### ⏸ Pausing the GitOps agent

Disable the CronJob:

```sh
kubectl patch cronjob/helmfile-gitops-agent -p '{"spec":{"suspend":true}}'
```

*The name of the CronJob can be different depending on the release name or the values you choose.*

### ▶ Resume the GitOps agent

Enable the CronJob again (the name will depend on the release name or values you chose):

```sh
kubectl patch cronjob/helmfile-gitops-agent -p '{"spec":{"suspend":false}}'
```

*The name of the CronJob can be different depending on the release name or the values you choose.*

## FAQ

### Is this real GitOps?

It sure enough is.
It fulfills [the principles](https://opengitops.dev):

1. You have a declarative file format: Helmfile
1. You store declarations in Git.
1. You have an agent continuously watching your manifests (the `git clone` init container).
1. You have an agent continuously rolling out your manifests (the `helmfile apply` container).

BUT this agent is a lot less mature and has way fewer features than more established tools like Flux and Argo.

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| createDeployKey.create | bool | `true` | Should we create a deploy key for you? If not, supply a secret with the keys `identity`, `identity.pub`, and `known_hosts` under `.gitClone.secret.name`. |
| createDeployKey.image | object | `{"pullPolicy":"IfNotPresent","repository":"fluxcd/flux-cli","tag":"v2.5.1"}` | Image configuration for creating the deploy key |
| createDeployKey.resources | object | `{"limits":{"cpu":"100m","memory":"50Mi"},"requests":{"cpu":"50m","memory":"50Mi"}}` | Resource management for creating the deploy key |
| createDeployKey.securityContext | object | `{"allowPrivilegeEscalation":false,"readOnlyRootFilesystem":true,"runAsNonRoot":true,"runAsUser":65534}` | Security context for creating the deploy key |
| cronjob.job.backoffLimit | int | `3` | How often to retry after failure? |
| cronjob.job.ttlSecondsAfterFinished | int | `900` | How long to keep finished jobs for debugging? |
| cronjob.schedule | string | `"*/5 * * * *"` | How often should the GitOps agent reconcile? (cron expression) |
| fullnameOverride | string | `""` |  |
| gitClone.cliArgs | string | `"--depth 1"` | CLI args to pass to `git clone` (shallow clone by default). To clone a specific branch or tag (but not a SHA), add "--branch NAME". |
| gitClone.image | object | `{"pullPolicy":"IfNotPresent","repository":"alpine/git","tag":"v2.47.2"}` | Image configuration for git clone |
| gitClone.repoUrl | string | `"git@gitlab.com:jscheytt/helmfile-experiments.git"` | REQUIRED: SSH URL to the Git repo you want to deploy |
| gitClone.resources | object | `{"limits":{"cpu":"100m","memory":"50Mi"},"requests":{"cpu":"50m","memory":"50Mi"}}` | Resource management for git clone |
| gitClone.secret.name | string | `"git-repo-credentials"` | Name of the secret containing the deploy key & known_hosts. |
| gitClone.securityContext.allowPrivilegeEscalation | bool | `false` |  |
| helmfile.args | list | `["apply","--suppress-secrets"]` | Which `helmfile` command do you want to run? |
| helmfile.image | object | `{"pullPolicy":"IfNotPresent","repository":"ghcr.io/helmfile/helmfile","tag":""}` | Image configuration for helmfile apply |
| helmfile.resources | object | `{"limits":{"cpu":"300m","memory":"200Mi"},"requests":{"cpu":"50m","memory":"200Mi"}}` | Resource management for helmfile apply |
| helmfile.securityContext | object | `{"allowPrivilegeEscalation":false}` | Security context for creating the deploy key |
| imagePullSecrets | list | `[]` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| serviceAccount.rules | list | `[{"apiGroups":["","*"],"resources":["*"],"verbs":["*"]}]` | What rules should the Role of the ServiceAccount have? |
| tolerations | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
