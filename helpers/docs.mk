HELPTEXT_HEADING := Documentation Targets:

.PHONY: docs
docs_files = README.md
docs_commit_message = docs: auto-generate README.md
docs: ## Generate README content. Optional parameters: should_commit
	helm-docs
	if [ "$(should_commit)" = "true" ]; then \
		git add $(docs_files); \
		git commit -m $(docs_commit_msg) || true; \
	fi
