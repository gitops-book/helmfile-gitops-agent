HELPTEXT_HEADING := Local Installation Targets:

.PHONY: install
install: ## Install dependencies to run make targets.
	brew install helm helmfile norwoodj/tap/helm-docs
