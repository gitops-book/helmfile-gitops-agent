HELPTEXT_HEADING := Release Targets:

.PHONY: release.run
release_run_workingdir = /workspace
release.run: ## Start container for semantic-release debugging.
	@echo "NOTE: Run these command after starting:"
	@echo "  $$ git config --global --add safe.directory $(release_run_workingdir)"
	@echo '  $$ semantic-release --dry-run --branches $$(git branch --show-current)'
	@echo
	docker run -it \
		--volume="$$PWD":$(release_run_workingdir) \
		--workdir=$(release_run_workingdir) \
		--entrypoint sh \
		$$(yq '.semantic-release.image' .gitlab-ci.yml) \

.PHONY: release.verify
registry_url = quay.io
release.verify: ## Verify conditions for release. Required arguments: REGISTRY_USERNAME (exported), REGISTRY_PASSWORD (exported). Optional arguments: registry_url
	$(if $(shell echo $$REGISTRY_USERNAME),,$(error Missing REGISTRY_USERNAME))
	$(if $(shell echo $$REGISTRY_PASSWORD),,$(error Missing REGISTRY_PASSWORD))
	@echo "$$REGISTRY_PASSWORD" | helm registry login --username "$$REGISTRY_USERNAME" --password-stdin $(registry_url)

.PHONY: release.prepare
release_files = Chart.yaml
release.prepare: ## Prepare release. Required arguments: new_version
	$(if $(new_version),,$(error Missing new_version))
	@# Update version in Chart.yaml
	yq -iP '.version = "$(new_version)"' Chart.yaml
	@# Update docs
	$(MAKE) docs
	@# Package chart
	helm package .

.PHONY: release.publish.chart
release_chart_path = oci://quay.io/gitops-book
release.publish.chart: ## Publish new Helm chart version. Required arguments: new_version. Optional arguments: release_chart_path
	$(if $(new_version),,$(error Missing new_version))
	helm push $(wildcard *-$(new_version).tgz) $(release_chart_path)
